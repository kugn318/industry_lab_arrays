package ictgradschool.industry.lab_arrays.ex03;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {

        // TODO Write your code here.

        int goal = (int) (Math.random() * 100);
        int guess = 0;

        System.out.println("Please enter your guess");
        guess = Integer.parseInt(Keyboard.readInput());

        while (guess != goal){
            if (guess > goal) {
                System.out.println("Too high, try again");
                guess = Integer.parseInt(Keyboard.readInput());
            } else if (guess < goal) {
                System.out.println("Too low, try again");
                guess = Integer.parseInt(Keyboard.readInput());
            }
            }
            System.out.println("Perfect!");
            System.out.println("Goodbye");

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
