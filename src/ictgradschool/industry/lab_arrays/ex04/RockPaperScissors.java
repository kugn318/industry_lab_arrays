package ictgradschool.industry.lab_arrays.ex04;

import ictgradschool.Keyboard;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    public static final int PAPER = 3;
    public static final int SCISSORS = 2;

    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.

        System.out.println("Hi! What is your name?");
        String name = Keyboard.readInput();
        System.out.println("1. Rock");
        System.out.println("2. Scissors");
        System.out.println("3. Paper");
        System.out.println("4. Quit");
        System.out.println("Enter choice: ");
        int userChoice = Integer.parseInt(Keyboard.readInput());
        displayPlayerChoice(name, userChoice);

        if (userChoice != 4){
            int computerChoice = (int)(Math.random()*3) + 1;
            if (computerChoice == ROCK){
                System.out.println("Computer chose Rock");
            }
            else if (computerChoice == SCISSORS){
                System.out.println("Computer chose Scissors");
            }
            else if (computerChoice == PAPER){
                System.out.println("Computer chose Paper");
            }


            String result = getResultString(userChoice,computerChoice);
            if(userChoice == computerChoice){
                System.out.println(result);
            }
            else if (userChoice != computerChoice) {
                 if (userWins(userChoice, computerChoice)) {
                    System.out.println(name + " wins because " + result);
                } else if (!userWins(userChoice, computerChoice)) {
                    System.out.println("computer" + " wins because " + result);
                }
            }
        }

    }


    public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)

        if (choice == 1){
            System.out.println(name + " chose Rock");
        }
        else if (choice == 2){
            System.out.println(name + " chose Scissors");
        }
        else if (choice == 3){
            System.out.println(name + " chose paper");
        }
        else if (choice == 4){
            System.out.println("Goodbye " + name + " Thanks for playing :))");
        }


    }

    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.

        if (playerChoice == ROCK){
            if (computerChoice == SCISSORS){
                return true;
            }
        }
        else if (playerChoice == PAPER){
            if (computerChoice == ROCK){
                return true;
            }
        }
        else if (playerChoice == SCISSORS){
            if (computerChoice == PAPER){
                return true;
            }
        }

        return false;
    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        if (playerChoice != computerChoice) {
            if (userWins(playerChoice, computerChoice)) {
                if (playerChoice == 1) {
                    return ROCK_WINS;
                } else if (playerChoice == 2) {
                    return SCISSORS_WINS;
                } else if (playerChoice == 3) {
                    return PAPER_WINS;
                }
            } else if (!userWins(playerChoice, computerChoice)) {
                if (computerChoice == 1) {
                    return ROCK_WINS;
                } else if (computerChoice == 2) {
                    return SCISSORS_WINS;
                } else if (computerChoice == 3) {
                    return PAPER_WINS;
                }
            }
        }

        return TIE;


    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
