package ictgradschool.industry.lab_arrays.ex05;

public class Pattern {

    private int repeat;
    private char symbol;

    public Pattern (int a, char b){
        repeat = a;
        symbol = b;
    }

    public void setNumberOfCharacters(int a) {
        repeat = a;
    }

    public int getNumberOfCharacters(){
        return repeat;
    }

    public String toString(){
        String returnString = "";

        for (int i = 0; i <repeat; i ++){
            returnString = returnString + Character.toString(symbol);
        }

        return returnString;
    }
}
